import jenkinsapi
from jenkinsapi.jenkins import Jenkins
import datetime
import sqlite3



class Jenkins_Service:

    def __init__(self, url, username, password):
        self.url = url
        self.username = username
        self.password = password

    def get_jenkins_instance(self):
        '''
        Returns a jenkins server instance
        :return: A server instance
        '''
        return Jenkins(self.url, username=self.username, password=self.password)

    def get_jobs_records(self):
        '''
        Creates a list of job records
        :return: A list of job records
        '''
        instance = self.get_jenkins_instance()
        instance_jobs  = instance.get_jobs()

        records = []
        for job_name, job_instance in instance_jobs:

            if job_instance.is_running():
                job_status = 'is_running'
            elif job_instance.is_queued():
                job_status = 'is_queued'
            else:
                job_status = 'idle'

            job_details = {}
            job_details['name'] = job_name
            job_details['status'] = job_status
            job_details['time'] = datetime.datetime.now().replace(microsecond=0)

            records.append(job_details)

        return records

    def get_connection(self):
        '''
        Opens a connection to a sqlite database
        :return: A database connection object
        '''
        return sqlite3.connect('job_db.db')


    def create_records_table(self, conn):
        '''
        Creates table that holds jobs records
        :param conn: A database connection object
        '''

        create_table_query = '''CREATE TABLE IF NOT EXISTS records
            (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            name TEXT NOT NULL,
            status TEXT NOT NULL,
            time TEXT NOT NULL);'''

        conn.execute(create_table_query)


    def insert_records(self, conn, records):
        '''
        Insert a list of records into the database
        :param conn: A database connection object
        :param records: The list of records to be inserted
        :return:
        '''
        #create table if it does already exist
        self.create_records_table(conn)

        for record in records:
            name = record['name']
            status = record['status']
            time = record['time']

            params = (name, status, time)

            conn.execute("INSERT INTO records (name, status, time) VALUES (?, ?,?)", params)

            conn.commit()

# Pass in url, username and password
url='http://localhost:8080'
username='admin'
password='admin'

conn = None

try:
    service = Jenkins_Service(url, username, password)
    jobs_records = service.get_jobs_records()

    conn = service.get_connection()
    service.insert_records(conn, jobs_records)

except Exception as e:
    print("Error occurred: %s" % str(e))

finally:
    if conn: conn.close()
    print('Done')
